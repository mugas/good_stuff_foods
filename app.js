var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    mongoose = require("mongoose"),
    passport = require("passport"),
    LocalStrategy = require("passport-local"),
    Recipe =  require("./models/recipes"), //not necessary to call Recipe but for simplicity is stays.
    Comment = require("./models/comment"),
    User = require("./models/user"),
    seedDB = require("./seeds")
    
    
    
//requiring routes    
var commentRoutes = require("./routes/comments"),
    recipeRoutes = require("./routes/recipes"),
    indexRoutes = require("./routes/index")
    


//mongoose.connect("mongodb://localhost/foodapp");
mongoose.connect("mongodb://mugas:almadiego11@ds143293.mlab.com:43293/goodstuff_foods");
//mongodb://mugas:almadiego11@ds143293.mlab.com:43293/goodstuff_foods

app.use(bodyParser.urlencoded({extended:true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

//seedDB(); //seed the database
    
//Passport configuration
app.use(require("express-session")({
    secret: "It's good stuff",
    resave: false,
    saveUnintialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));// the autenthticate is what comes with the model
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


app.use(function(req, res, next){
   res.locals.currentUser = req.user;
   next();
});


app.use("/", indexRoutes);
app.use("/recipes", recipeRoutes);
app.use("/recipes/:id/comments", commentRoutes);

app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Food App on the go")
});