var express = require("express");
var router = express.Router({mergeParams: true});
var Recipe = require("../models/recipes");
var Comment = require("../models/comment");


//===================
//COMMENTS ROUTES
//===================   

//comments new
router.get("/new", isLoggedIn, function(req,res){
    //find recipe by id
    Recipe.findById(req.params.id, function(err, recipe){
        if(err){
            console.log(err);
        } else {
        res.render("comments/new", {recipe: recipe});       
        }
    })
     
})


//comments create
router.post("/", isLoggedIn, function(req,res){
    //look up recipe using id
    Recipe.findById(req.params.id, function(err,recipe){
       if(err){
           console.log(err)
           res.redirect("/recipes");
       } else {
           Comment.create(req.body.comment, function(err,comment){
               if(err){
                   console.log(err);
               } else {
                   //add username and id to comment
                   comment.author.id = req.user._id;
                   comment.author.username = req.user.username;
                   //save comment 
                   comment.save();
                   recipe.comments.push(comment);
                   recipe.save();
                   res.redirect('/recipes/'+ recipe._id);
               }
           })
       }
    });
    //create new comment
    //connnect new comment to recipe
    //redirect recipe show page
    
});


//middleware
function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
}

module.exports = router;
