var mongoose = require('mongoose'); 

var recipeSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String,
    comments: [
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:"Comment"
        }
        ]
});

module.exports =  mongoose.model("Recipe", recipeSchema);  // Recipe here is the name of the dabase in pluraldb
