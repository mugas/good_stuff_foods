var mongoose = require("mongoose");
var Recipe = require("./models/recipes");
var Comment = require("./models/comment");


var data=[
    {name:"First Recipe", 
    image:"https://images.unsplash.com/photo-1516865131505-4dabf2efc692?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=380a2b91ee844c3d6ee349952f672eaf&auto=format&fit=crop&w=890&q=80",
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or" 
    },
    {
    name:"Second Recipe", 
    image:"https://images.unsplash.com/photo-1505253668822-42074d58a7c6?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=173652e77ca58a61c86233ca20d89d6a&auto=format&fit=crop&w=334&q=80",
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or" 
    },
    {
    name:"Third Recipe", 
    image:"https://images.unsplash.com/photo-1505252929202-c4f39cda4d49?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b50befed71b9343dcc112253e7aeb5ee&auto=format&fit=crop&w=334&q=80",
    description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or" 
    },
    
    ]


 function seedDB(){
     //Remove all campgrounds
     Recipe.remove({}, function(err){
    if(err){
        console.log(err);
    }
        console.log("removed recipes");
         //add a few recipes
    data.forEach(function(seed){
        Recipe.create(seed, function(err,recipe){
            if(err){
                console.log(err)
            } else {
            console.log("added a recipe");
            
            //create a comment
            Comment.create(
                {
                    text:"This is great",
                    author:"Homer"
                },function(err, comment){
                    if(err){
                        console.log(err);
                    }else {
                        recipe.comments.push(comment);
                    recipe.save();
                    console.log("create a new comment");
                    }
                }); 
                    }
                   
        
        });
    });
    });
    
   
    //add a few comments
 }
 
 module.exports = seedDB;

