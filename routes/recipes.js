var express = require("express");
var router = express.Router();
var Recipe = require("../models/recipes");


//Index - Display a list of all recipes
router.get("/", function(req,res){
        //Get all recipes from DB
        Recipe.find({}, function(err, allRecipes){
            if(err){
                console.log(err);
            } else {
                res.render("recipes/index", {recipes:allRecipes}); // The first "recipes" we can call it anything, the second is the data
            }
        });
});

//Create - Add new recipe
router.post("/", function(req,res){
    //get data from the form and add to recipes array
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    var newRecipe = {name: name, image: image, description: desc}
    //create new receipe and save to DB
    Recipe.create(newRecipe, function(err, newlyCreated){
       if(err){
           console.log(err);
       } else {
            //redirect back to recipes page
            res.redirect("/recipes");   
       }
    });
   
})

//NEW - Show form to create recipe
router.get("/new", function(req,res){
    res.render("recipes/new");
});

//SHOW - Show more info about one campground
router.get("/:id", function(req,res){
    //find the campground with the provided ID
    Recipe.findById(req.params.id).populate("comments").exec(function(err, foundRecipe){
       if(err){
           console.log(err)
       } else {
           console.log(foundRecipe)
           //render show template with that campground
    res.render("recipes/show", {recipe: foundRecipe});
       }
    });
})


module.exports = router;